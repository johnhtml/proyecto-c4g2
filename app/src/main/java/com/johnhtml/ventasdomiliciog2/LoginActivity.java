package com.johnhtml.ventasdomiliciog2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    TextInputLayout tilEmail;
    TextInputEditText etEmail;
    TextInputLayout tilPassword;
    TextInputEditText etPassword;

    AppCompatButton btnLogin;
    AppCompatButton btnFacebook;
    AppCompatButton btnGoogle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initUI();
    }

    private void initUI(){
        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> onLoginClick());

        btnFacebook = findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener(v -> onLoginFacebook());

        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener(v -> onLoginGoogle());
    }

    private void onLoginClick(){
        // TODO
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void onLoginFacebook(){
        // TODO
        String email = Objects.requireNonNull(etEmail.getText()).toString();
        if (email.isEmpty()) {
            tilEmail.setError("Email es obigatorio");
        }

        //Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        //startActivity(intent);
    }

    private void onLoginGoogle(){
        // TODO
        tilEmail.setError("u.u");
        //Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        //startActivity(intent);
    }
}